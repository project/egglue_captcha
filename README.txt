
Egglue Semantic CAPTCHA for Drupal
====================

The Egglue Semantic CAPTCHA module uses the Egglue Semantic CAPTCHA web
service to improve the CAPTCHA system with semantics. Unlike conventional
CAPTCHA challenges, Egglue uses common-knowledge to prevent automated
access.

For more information on what Egglue Semantic CAPTCHA is, please visit:

       http://www.egglue.com


INSTALLATION
------------

1. Extract the Egglue Semantic CAPTCHA module to your local favourite
   modules directory (sites/all/modules).
   

CONFIGURATION
-------------
   
2. Enable Egglue Semantic CAPTCHA and CAPTCHA modules in:
       admin/build/modules

3. Select Egglue Semantic CAPTCHA as the challenge type for your forms in:
       admin/user/captcha/captcha
   

THANK YOU
---------

 * Thank you goes to the Egglue team for all their help and their
   amazing semantic CAPTCHA solution!
       http://www.egglue.com
