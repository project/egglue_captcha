<?php

/**
 * @file
 * Code required the Egglue module
 */

/**
 * Let the user know that the Egglue PHP library is not installed
 */   
function _egglue_captcha_library_not_found() {
  global $calledalready;
  if(!$calledalready) {
    $calledalready = TRUE;
    drupal_set_message(t('The <a href="@url" target="_blank">Egglue PHP library</a> was not found. Please install it into %eggluedir.', array('@url' => 'http://code.google.com/p/egglue/downloads', '%eggluedir' => drupal_get_path('module', 'egglue_captcha') . '/egglue')), 'error');

    if(!function_exists('egglue_get_html')) {
      function egglue_get_html() {
        return NULL;
      }
    }
    if(!function_exists('egglue_check_answer')) {
      function egglue_check_answer($remoteip, $challenge, $response) {
        return NULL;
      }
    }

  }
} // function _egglue_captcha_library_not_found()
